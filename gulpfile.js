'use strict';

var gulp = require('gulp');
var concat = require('gulp-concat');
var eventStream = require('event-stream');
var order = require('gulp-order');
var sass = require('gulp-ruby-sass');
var sourcemaps = require('gulp-sourcemaps');
var templateCache = require('gulp-angular-templatecache');
var jshint = require('gulp-jshint');
var handlebars = require('gulp-compile-handlebars');
var uglify = require('gulp-uglify');
var watch = require('gulp-watch');
var del = require('del');
var stripDebug = require('gulp-strip-debug');
var minifyCss = require('gulp-minify-css');
var gutil = require('gulp-util');
var jip = require('jasmine-istanbul-phantom');

var config = require('./config.js');

gulp.task('default',function(){
	gulp.start(['css', 'js', 'markup', 'fakeApi']);
});

var watchConsoleMessage = function(e){
	console.log('[watch] ' + e.event + ' -> ' + e.history[0]);
};

// Task for watch changed files and run specific tasks
gulp.task('watch',function(){
	gulp.start('default');

	// whatch changes for sass files
	watch(['src/scss/**/*.scss'], function(event){
		watchConsoleMessage(event);
		gulp.start('css');
	});

	// whatch changes for js files and also angular templates
	watch([
			'src/**/*.js', 
			'lib/**/*.js', 
			'src/directives/**/*.html',
			'src/views/**/*.html'
		],
		function(event){
			watchConsoleMessage(event);
			gulp.start('js');
	});

	// whatch changes for html files, not for angular templates
	watch([
			'src/**/*.html', 
			'!src/directives/**/*.html', 
			'!src/views/**/*.html'
		], function(event){
		watchConsoleMessage(event);
		gulp.start('markup');
	});

});

// js files task
// group js files in libs and src, also add a third group for angular templates
// minifying only js src and ignore libs in order to reduce build time
gulp.task('js', ['lint'], function () {

	// stream js lib minified files
	var libFiles = gulp.src([
			'lib/js/angular.min.js',
			'lib/js/**/*.js'
		])
		.pipe(config.dest == 'build' ? gutil.noop() : sourcemaps.init())
		.pipe(concat('libFiles.js'));

	// stream js src unminified files with logs
	var appFiles = gulp.src(['src/**/*.js'])
		.pipe(config.dest == 'build' ? gutil.noop() : sourcemaps.init())

		//minify files and remove logs for build
		.pipe(config.dest == 'build' ? uglify() : gutil.noop())
		.pipe(config.dest == 'build' ? stripDebug() : gutil.noop())
		
		.pipe(concat('appFiles.js'));

	// stream angular templates compiled to js
	var angularTemplates = gulp.src([
			'src/views/**/*.html', 
			'src/directives/**/*.html'
		])
		.pipe(config.dest == 'build' ? gutil.noop() : sourcemaps.init())
		.pipe(templateCache({
			module:'walmartApp',
			filename: 'angularTemplates.js'
		}));

	// push debug js version with js src files unminifed
	eventStream.concat([libFiles, appFiles, angularTemplates])
		.pipe(order([
			'libFiles.js',
			'appFiles.js',
			'angularTemplates.js'
		]))
		.pipe(concat('app.min.js'))
		.pipe(config.dest == 'build' ? gutil.noop() : sourcemaps.write('map/'))
		.pipe(gulp.dest(config.dest + '/js/'));
});

gulp.task('css', function () {
	// clean css builds
	del([config.dest+'/css/*']);

	// clone css libs and move them to build and debug folders
	gulp.src(['lib/css/**/*.css'])
		.pipe(gulp.dest(config.dest+'/css/'));

	// compile sass files to css
	sass('src/scss/app.scss', {
		sourcemap: config.dest == 'build' ? false : true,
		style: config.dest == 'build' ? 'compressed' :  'nested'
	})
		.pipe(config.dest == 'build' ? gutil.noop() : sourcemaps.write('map/'))

		// minify css for build
		.pipe(config.dest == 'build' ? minifyCss() : gutil.noop())
		.pipe(gulp.dest(config.dest + '/css'));
	
});

gulp.task('markup',function(){
	gulp.src(['src/index.html'])
		.pipe(handlebars(config))
		.pipe(gulp.dest(config.dest));
});

// check if js src files are following the .jshintrc configuration rules
// used as a dependencie of js task build
gulp.task('lint', function() {
	return gulp.src(['src/**/*.js'])
		.pipe(jshint())
		.pipe(jshint.reporter('default'))
  	.pipe(jshint.reporter('fail'));
});


gulp.task('fakeApi', function(){
	return gulp.src(['_api_fake/*.json'])
		.pipe(gulp.dest(config.dest + '/_api_fake/'));
});

gulp.task('test', function(){
	jip({
		'lib' : [
			'test/lib/angular.js',
			'test/lib/angular-mocks.js',
			'test/lib/angular-route.js'
		]
	});
});