'use strict';

(function(){

	var app = angular.module('walmartApp', [
			'ngRoute',
			'walmartApp.views.products',
			'walmartApp.views.addresses',
			'walmartApp.views.user',
			'walmartApp.directives.navigation',
		]);
	
	app.config(['$routeProvider', function($routeProvider) {
			$routeProvider

				.when('/products/', {
					templateUrl: 'products/products.html',
					controller: 'ProductsCtrl as products'
				})

				.when('/addresses/', {
					templateUrl: 'addresses/addresses.html',
					controller: 'AddressesCtrl as addresses'
				})

				.when('/user/', {
					templateUrl: 'user/user.html',
					controller: 'UserCtrl as user'
				})

		  	.otherwise({redirectTo: '/products/'});

	}]);

})();
