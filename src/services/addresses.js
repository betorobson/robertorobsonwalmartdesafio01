'use strict';

angular.module('walmartApp.services.addresses',[])
.factory('addressesService', ['$http', function($http){

  var service = {
    get : function(callback){
      $http.get('/_api_fake/addresses.json').success(function(data){
        if(typeof callback === 'function')
        	callback(data.addresses || {});
        else
        	console.error('addressesService callback must be a function');
      });
    }
  };

  return service;

}]);
