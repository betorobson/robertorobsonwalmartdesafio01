'use strict';

angular.module('walmartApp.services.products',[])
.factory('productsService', ['$http', function($http){

  var service = {
    get : function(callback){
      $http.get('/_api_fake/products.json').success(function(data){
        if(typeof callback === 'function')
        	callback(data.products ? data : {});
        else
        	console.error('productsService callback must be a function');
      });
    }
  };

  return service;

}]);
