'use strict';

angular.module('walmartApp.services.user',[])
.factory('userService', ['$http', function($http){

  var service = {
    get : function(callback){
      $http.get('/_api_fake/user.json').success(function(data){
        if(typeof callback === 'function')
        	callback(data.user || {});
        else
        	console.error('userService callback must be a function');
      });
    }
  };

  return service;

}]);
