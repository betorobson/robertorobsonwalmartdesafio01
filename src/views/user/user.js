'use strict';

(function(){
	var app = angular.module('walmartApp.views.user', 
		['walmartApp.services.user']);
	
	app.controller('UserCtrl', 
		['$scope', 'userService',
		function($scope, userService) {

		console.log('walmartApp.views.user loaded!');

			$scope.data = {};

			userService.get(function(data){
				console.log('Data from userService');
				console.log(data);
				$scope.data = data;
			});

	}]);
})();
