'use strict';

(function(){
	var app = angular.module('walmartApp.views.addresses', 
		['walmartApp.services.addresses']);

	app.controller('AddressesCtrl', 
		['$scope', 'addressesService', 
		function($scope, addressesService) {

		console.log('walmartApp.views.addresses loaded!');

			$scope.data = {};

			addressesService.get(function(data){
				console.log('Data from addressesService');
				console.log(data);
				$scope.data = data;
			});

	}]);
})();
