'use strict';

(function(){
	var app = angular.module('walmartApp.views.products', 
		['walmartApp.services.products']);

	app.controller('ProductsCtrl', 
		['$scope', 'productsService', 
		function($scope, productsService) {

			console.log('walmartApp.views.products loaded!');

			$scope.data = {};

			productsService.get(function(data){
				console.log('Data from productsService');
				console.log(data);
				$scope.data = data;
			});

	}]);
})();
