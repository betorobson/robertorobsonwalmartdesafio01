'use restrict';

(function(){

	var app = angular.module('walmartApp.directives.navigation', []);

	app.directive('walmartNavigation', function() {
		return {
			restrict: 'E',
			templateUrl: 'navigation/navigation.html',
			controllerAs: 'navigation',
			controller: 'navigationCtrl'
		};
	});

	app.controller('navigationCtrl', ['$scope', function($scope){
		$scope.itemActive = 0;
		console.log('walmartApp.directives.navigation loaded!');
	}]);

})();
