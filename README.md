# Desafio Walmart para o Roberto Robson

Este web app é o desafio do Walmart sendo constituido de 3 áreas:
- Produtos do usuário
- Endereços para entrega
- Perfil do usuário

## Requisitos

### NodeJS
Command line tool baseado em javascript.
Neste projeto será utilizado para rodar as tarefas de build e testes.

Também serve para instalar os pacotes baseados em node através do [npm][npm].

Por se tratar de um web app, os pacotes devem ser instalados apenas para desenvolvedores, para isso use sempre `--save-dev` no final de cada comando de instalação

Os pacotes serão listados no arquivo `package.json`, assim quando um desenvolvedor baixar o source, basta ele instalar o projeto que todas as dependências serão instaladas.

Download e instalação [NodeJS][nodejs]

### Gulp
Task runner para automatizar os processos de build, debug e testes.
As tasks são escritas dentro do gulpfile.js que fica na raiz.

Instalação 
```
npm install gulp --save-dev
```

Plugins para auxiliar as taks em Gulp podem ser encontradas no [Gulp Plugins][gulpplugins].

*Importante: É preciso ter antes instalado o gulp no ambiente global, caso não tenha instale com o comando abaixo:
```
npm -g install gulp
```

Documentação: [Gulp][gulp]

### JsHint
JsHint é utilizado para manter o código do projeto o mais padronizado possível.
Algumas IDE como o Sublime conseguem utilizar esse arquivo para mostrar durante a codificação qualquer erro de sintaxe ou desconforme em relação ao padrão de coódigo adotado no projeto.

Também é utilizado como parte importante do processo de build, garantindo que o build só aconteça se os javascripts estejam padronizados e sem erros de sintaxe.
O arquivo `.jshintrc` deve ficar na raiz. 

Download [jshint][jshint]

### AngularJS
Angular é um framework para desenvolver web apps baseado em camadas (MVVM) tendo controllers, views, services, etc.

O desenvolvimento de um Single Page App é facilitado já que tudo pode rodar em uma única página HTML sem reload.

Outra vantagem é o uso do ngRoute, um componente do Angular que permite definir as rotas do app baseado na URL. Dando ao usuário a percepção de uma navegação fluida mantendo historicos.

Documentação: [AngularJS][angular]

### SASS
SASS é um pré-processadore de CSS. Fica mais fácil manter o CSS organizado e manutenível.
Com ele podemos ter os recursos abaixo:

* Includes, diferente do include padrão de CSS, partes do código ficam em arquivos separados e depois de compilados são únificados em um único arquivo CSS;
* Variáveis, assim é possível reaproveitar valores de propriedades como cores, fonts, etc. Sendo assim podemos ter um arquivo include chamado `_variables.scss`
* mixin, um recurso muito importante do SASS que permite reaproveitamento de trechos de código com diretivas, proriedades e elementos. Por exemplo, fica mais fácil implementar a propriedade `border-radius` mantendo compatibilidade entre navegadores. 

Neste projeto o SASS será compilado através de uma task no Gulp no processo de build do projeto.
Documentação [SASS][sass]

### CSS Reset
Alguns CSS Reset foram utilizados do [cssreset][cssreset] para remover stilos padrões e manter o site com a mesma aparência entre navegadores.

## Instalação
Para instalar as dependencias do projeto execute:
```
npm install
```

## Gerando Build
O Gulp é utilizado para gerar o build através do commando abaixo:
```
gulp default
```
Durante o desenvolvimento pode ser utilizado a task `watch` para realizar build a cada arquivo alterado no source
```
gulp watch
```
O processo de build pode ser configurado para o diretório `build` ou `debug`. 
Nessa versão `debug` são mantidos os consoles logs e também os source maps de CSS e Javascript.
Para alterar o destino do build é necessário alterar o arquivo config.js no nó `dest`

## executando o projeto
Executar o build:
```
serve build
```

Executar o debug
```
serve debug
```
* Para executar ambos sugiro a instalação do pacote [Serve][serve] que facilmente sobe um webserve.

[nodejs]: https://nodejs.org/
[npm]: https://www.npmjs.com/
[gulp]: http://gulpjs.com/
[gulpplugins]: http://gulpjs.com/plugins/
[jshint]: http://jshint.com/
[angular]: http://angularjs.org
[sass]: http://sass-lang.com
[cssreset]: http://cssreset.com
[serve]: https://github.com/tj/serve