/* global describe, beforeEach, it, expect, inject */

/*
describe('walmartApp.views.user', function() {
  beforeEach(module('walmartApp.views.user'));

  var $controller, userService, data;

  beforeEach(inject(function(_$controller_){
    $controller = _$controller_;

		userService = {
			get : function(callback){
				data = {a:1,b:2};
				callback(data);
			}
		};
    
  }));

  describe('UserCtrl', function() {

    it('Data and $scope.data must be the same', function() {
      var $scope = {};
      var controller = $controller('UserCtrl', { $scope: $scope, userService: userService });
      expect($scope.data).toEqual(data);
    });

  });

});
*/


describe('walmartApp.views.user', function() {

  var scope, Controller, userService, data;
  beforeEach(module('walmartApp.views.user'));

  beforeEach(inject(function($rootScope, $controller) {
    scope = $rootScope.$new();

    userService = {
      get : function(callback){
        data = {a:1,b:2};
        callback(data);
      }
    };

    Controller = $controller('UserCtrl', {
      $scope: scope,
      userService: userService
    });
  }));

  it('scope.data must be equal to data', function() {
    expect(scope.data).toEqual(data);
  });

});

/////////////////////////

describe('walmartApp.views.products', function() {

  var scope, Controller, productsService, data;
  beforeEach(module('walmartApp.views.products'));

  beforeEach(inject(function($rootScope, $controller) {
    scope = $rootScope.$new();

    productsService = {
      get : function(callback){
        data = {a:1,b:2};
        callback(data);
      }
    };

    Controller = $controller('ProductsCtrl', {
      $scope: scope,
      productsService: productsService
    });
  }));

  it('scope.data must be equal to data', function() {
    expect(scope.data).toEqual(data);
  });

});

/////////////////////////

describe('walmartApp.views.addresses', function() {

  var scope, Controller, addressesService, data;
  beforeEach(module('walmartApp.views.addresses'));

  beforeEach(inject(function($rootScope, $controller) {
    scope = $rootScope.$new();

    addressesService = {
      get : function(callback){
        data = {a:1,b:2};
        callback(data);
      }
    };

    Controller = $controller('AddressesCtrl', {
      $scope: scope,
      addressesService: addressesService
    });
  }));

  it('scope.data must be equal to data', function() {
    expect(scope.data).toEqual(data);
  });

});

/////////////////////////

describe('walmartApp.services.user', function () {
  var service, httpBackend;

  beforeEach(module('walmartApp.services.user'));

  beforeEach(inject(function (_userService_, $httpBackend) {
    service = _userService_;
    httpBackend = $httpBackend;    
  }));

  it('should get user data', function () {
    var responseData = {user: {name: 'foo'}};
    httpBackend.whenGET('/_api_fake/user.json').respond(responseData);

    service.get(function(data){
      expect(data).toEqual(responseData.user);
    });
    httpBackend.flush();
  });

  it('should call callback and give it a empty object becase json has not `user` node', function () {
    httpBackend.whenGET('/_api_fake/user.json').respond({error:true});

    service.get(function(data){
      expect(data).toEqual({});
    });
    httpBackend.flush();
  });

  it('should run callback', function () {
    var callback = {func:function(){}};
    spyOn(callback, 'func');
    
    httpBackend.whenGET('/_api_fake/user.json').respond({user:'foo'});

    service.get(callback.func);
    httpBackend.flush();

    expect(callback.func).toHaveBeenCalled();
    
  });

  it('should call console.error when callback is not a function', function () {
    spyOn(console, 'error');
    
    httpBackend.whenGET('/_api_fake/user.json').respond({user:'foo'});

    service.get();
    httpBackend.flush();

    expect(console.error).toHaveBeenCalled();
    
  });

});

/////////////////////////

describe('walmartApp.services.products', function () {
  var service, httpBackend;

  beforeEach(module('walmartApp.services.products'));

  beforeEach(inject(function (_productsService_, $httpBackend) {
    service = _productsService_;
    httpBackend = $httpBackend;    
  }));

  it('should get products data', function () {
    var responseData = {products: [{},{},{}]};
    httpBackend.whenGET('/_api_fake/products.json').respond(responseData);

    service.get(function(data){
      expect(data).toEqual(responseData);
    });
    httpBackend.flush();
  });

  it('should call callback and give it a empty object becase json has not `products` node', function () {
    httpBackend.whenGET('/_api_fake/products.json').respond({error:true});

    service.get(function(data){
      expect(data).toEqual({});
    });
    httpBackend.flush();
  });

  it('should run callback', function () {
    var callback = {func:function(){}};
    spyOn(callback, 'func');
    
    httpBackend.whenGET('/_api_fake/products.json').respond({products: [{},{},{}]});

    service.get(callback.func);
    httpBackend.flush();

    expect(callback.func).toHaveBeenCalled();
    
  });

  it('should call console.error when callback is not a function', function () {
    spyOn(console, 'error');
    
    httpBackend.whenGET('/_api_fake/products.json').respond({products: [{},{},{}]});

    service.get();
    httpBackend.flush();

    expect(console.error).toHaveBeenCalled();
    
  });

});

/////////////////////////

describe('walmartApp.services.products', function () {
  var service, httpBackend;

  beforeEach(module('walmartApp.services.addresses'));

  beforeEach(inject(function (_addressesService_, $httpBackend) {
    service = _addressesService_;
    httpBackend = $httpBackend;    
  }));

  it('should get products data', function () {
    var responseData = {addresses: [{},{},{}]};
    httpBackend.whenGET('/_api_fake/addresses.json').respond(responseData);

    service.get(function(data){
      expect(data).toEqual(responseData.addresses);
    });
    httpBackend.flush();
  });

  it('should call callback and give it a empty object becase json has not `products` node', function () {
    httpBackend.whenGET('/_api_fake/addresses.json').respond({error:true});

    service.get(function(data){
      expect(data).toEqual({});
    });
    httpBackend.flush();
  });

  it('should run callback', function () {
    var callback = {func:function(){}};
    spyOn(callback, 'func');
    
    httpBackend.whenGET('/_api_fake/addresses.json').respond({addresses: [{},{},{}]});

    service.get(callback.func);
    httpBackend.flush();

    expect(callback.func).toHaveBeenCalled();
    
  });

  it('should call console.error when callback is not a function', function () {
    spyOn(console, 'error');
    
    httpBackend.whenGET('/_api_fake/addresses.json').respond({addresses: [{},{},{}]});

    service.get();
    httpBackend.flush();

    expect(console.error).toHaveBeenCalled();
    
  });

});

